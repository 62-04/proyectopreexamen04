package Controlador;

import Modelo.*;
import vista.*;
import java.awt.event.*;
import java.util.logging.*;
import javax.swing.*;
import javax.swing.table.TableModel;

public class Controlador implements ActionListener {
    private vistaIngreso vistaIngreso;
    private vistaLista vistaLista;
    private vistaRegistro vistaRegistro;
    private Usuarios usu;
    private dbManejador Manejador;
    private dbUsuarios dbUsu;
    private boolean isInsertar = true;
  
    public Controlador(vistaIngreso vistaIngreso, vistaLista vistaLista, vistaRegistro vistaRegistro, Usuarios usu, dbManejador Manejador, dbUsuarios dbUsu) {
        this.vistaIngreso = vistaIngreso;
        this.vistaLista = vistaLista;
        this.vistaRegistro = vistaRegistro;
        this.usu = usu;
        this.Manejador = Manejador;
        this.dbUsu = dbUsu;
        
        vistaIngreso.btnIngresar.addActionListener(this);
        vistaIngreso.btnRegistrarse.addActionListener(this);
        vistaIngreso.jpContraseña.addActionListener(this);
        vistaIngreso.txtUsuarios.addActionListener(this);
        
        vistaLista.btnCerrarTabla.addActionListener(this);
        
        vistaRegistro.txtCorreo.addActionListener(this);
        vistaRegistro.btnCerrar.addActionListener(this);
        vistaRegistro.btnGuardar.addActionListener(this);
        vistaRegistro.btnLimpiar.addActionListener(this);
        vistaRegistro.jpContraseñaR.addActionListener(this);
        vistaRegistro.jpContraseñaaRC.addActionListener(this);
        vistaRegistro.txtUsuariosR.addActionListener(this);
    }
    
    private void iniciarVista() {
        try {
            vistaLista.jtbUsuarios.setModel((TableModel) dbUsu.listar());
            vistaIngreso.setTitle("Ingreso");
            vistaIngreso.setSize(280 , 220);
            vistaIngreso.setVisible(true);
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(vistaIngreso, "Error al iniciar: " + ex.getMessage());
            Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
  
    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == vistaIngreso.btnIngresar) {
            String username = vistaIngreso.txtUsuarios.getText();
            String password = String.valueOf(vistaIngreso.jpContraseña.getPassword());

            // Realizar validación de inicio de sesión aquí
            if (dbUsu.validarCredenciales(username, password)) {
                JOptionPane.showMessageDialog(vistaIngreso, "Bienvenido: " + username);
                vistaLista.setVisible(true);
                vistaIngreso.setVisible(false);
            } else {
                 // Credenciales inválidas, mostrar mensaje de error
                JOptionPane.showMessageDialog(vistaIngreso, "Credenciales inválidas", "Error de inicio de sesión", JOptionPane.ERROR_MESSAGE);
            }
        } else if (e.getSource() == vistaIngreso.btnRegistrarse) {
            vistaIngreso.setVisible(false);
            vistaRegistro.setTitle("Registro");
            vistaRegistro.setSize(400, 400);
            vistaRegistro.setVisible(true);

        } else if (e.getSource() == vistaLista.btnCerrarTabla) {
            if (JOptionPane.showConfirmDialog(vistaLista, "¿Desea cerrar?", "Cerrar", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
                vistaLista.setVisible(false);
                vistaLista.dispose();
                System.exit(0);
            } 
        } else if (e.getSource() == vistaRegistro.btnGuardar) {
            String password1 = vistaRegistro.jpContraseñaR.getText();
            String password2 = vistaRegistro.jpContraseñaaRC.getText();
            String correo = vistaRegistro.txtCorreo.getText();
            String usuario = vistaRegistro.txtUsuariosR.getText();
            if (password1.equals(password2)) {
                if (correo.isEmpty()) {
                    JOptionPane.showMessageDialog(null, "El correo está vacío.", "Error", JOptionPane.ERROR_MESSAGE);
                } else if (usuario.isEmpty()) {
                    JOptionPane.showMessageDialog(null, "El nombre de usuario está vacío.", "Error", JOptionPane.ERROR_MESSAGE);
                } else if (password1.isEmpty()) {
                    JOptionPane.showMessageDialog(null, "La contraseña está vacía.", "Error", JOptionPane.ERROR_MESSAGE);
                } else {
                     // Verificar si el usuario ya existe en la base de datos
                    if (dbUsu.usuarioExiste(usuario)) {
                        JOptionPane.showMessageDialog(null, "Ya existe este usuario.", "Error", JOptionPane.ERROR_MESSAGE);
                    } else {
                        usu.setContra(password1);
                        usu.setCorreo(correo);
                        usu.setNombre(usuario);

                        if (this.isInsertar) {
                            try {
                                if (this.Manejador.conectar()) {
                                    dbUsu.insertar(usu);
                                    vistaLista.jtbUsuarios.setModel((TableModel) dbUsu.listar());
                                    JOptionPane.showMessageDialog(vistaRegistro, "Se agregó con exito");
                                    limpiar();
                                    vistaRegistro.setVisible(false);
                                    vistaIngreso.setVisible(true);
                                    this.Manejador.desconectar();
                                } else {
                                    JOptionPane.showMessageDialog(vistaRegistro, "No se pudo establecer la conexión con la base de datos.", "Error", JOptionPane.ERROR_MESSAGE);
                                }
                            } catch (Exception ex) {
                                JOptionPane.showMessageDialog(vistaRegistro, "Error al agregar: " + ex.getMessage());
                                Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
                            }
                        }
                    }
                }
            } else {
                JOptionPane.showMessageDialog(null, "Las contraseñas no son las mismas.", "Error", JOptionPane.ERROR_MESSAGE);
            }
        } else if (e.getSource() == vistaLista.btnCerrarTabla) {
            if (JOptionPane.showConfirmDialog(vistaLista, "¿Desea cerrar?", "Cerrar", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
                vistaLista.setVisible(false);
                vistaLista.dispose();
                System.exit(0);
            }
        } else if (e.getSource() == vistaRegistro.btnLimpiar) {
            limpiar();
        } else if (e.getSource() == vistaRegistro.btnCerrar) {
            if (JOptionPane.showConfirmDialog(vistaRegistro, "¿Desea cerrar?", "Cerrar", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
                vistaRegistro.setVisible(false);
                vistaIngreso.setTitle("Ingreso");
                vistaIngreso.setSize(280, 220);
                vistaIngreso.setVisible(true);
            }
        }

    }
    
    public void limpiar() {
        vistaRegistro.txtCorreo.setText("");
        vistaRegistro.jpContraseñaR.setText("");
        vistaRegistro.jpContraseñaaRC.setText("");
        vistaRegistro.txtUsuariosR.setText("");
    }
    
    public static void main(String[] args) throws Exception {
        dbUsuarios db = new dbUsuarios();
        dbManejador cot = new dbManejador();
        vistaLista Lista = new vistaLista(new JFrame(), false);
        Lista.setTitle("Lista");
        Lista.setSize(500, 300);
        Lista.setVisible(false);
        
        vistaRegistro Registro = new vistaRegistro(new JFrame(), false);
        Registro.setTitle("Registro");
        Registro.setSize(400, 300);
        Registro.setVisible(false);
        
        vistaIngreso Ingreso = new vistaIngreso(new JFrame(), true);
        var usuarios = new Usuarios();
        Controlador con = new Controlador(Ingreso, Lista, Registro, usuarios, cot, db);
        con.iniciarVista();
    }
}
