package Modelo;

public class Usuarios {
    private int idUsuarios;
    private String nombre;
    private String correo;
    private String contra;

    public Usuarios(int idUsuarios, String nombre, String correo, String contra) {
        this.idUsuarios = idUsuarios;
        this.nombre = nombre;
        this.correo = correo;
        this.contra = contra;
    }

    public Usuarios(Usuarios user) {
        this.idUsuarios = user.idUsuarios;
        this.nombre = user.nombre;
        this.correo = user.correo;
        this.contra = user.contra;
    }

    public Usuarios() {
        this.idUsuarios = 0;
        this.nombre = "";
        this.correo = "";
        this.contra = "";
    }

    public void setIdUsuarios(int idUsuarios) {
        this.idUsuarios = idUsuarios;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public void setContra(String contra) {
        this.contra = contra;
    }

    public int getIdUsuarios() {
        return idUsuarios;
    }

    public String getNombre() {
        return nombre;
    }

    public String getCorreo() {
        return correo;
    }

    public String getContra() {
        return contra;
    }
    
    
}
